<?php
//includes
include('lib/XmlStringStreamer.php');
include('lib/XmlStringStreamer/ParserInterface.php');
include('lib/XmlStringStreamer/StreamInterface.php');
include('lib/XmlStringStreamer/Parser/StringWalker.php');
include('lib/XmlStringStreamer/Parser/UniqueNode.php');
include('lib/XmlStringStreamer/Stream/File.php');
include('lib/XmlStringStreamer/Stream/Stdin.php');
include('lib/xml2json.php');
require('lib/mailer/swift_required.php');
require('lib/Utils.php');
require('lib/elasticsearch.php');



//Config
include("config.php");

//Bootstrap app
$di = new \Phalcon\Di\FactoryDefault();
$app = new \Phalcon\Mvc\Micro($di);

//Set logger
$logger = new \Phalcon\Logger\Adapter\File(LOG_FILE);

//Set error handler
set_error_handler(function ($errno, $errstr, $errfile, $errline) use ($app, $logger) {
    $logger->error("$errno, $errstr, $errfile, $errline");
});

//Set falar error logging
register_shutdown_function(function () use ($logger) {
    $error = error_get_last();
    if(isset($error['type'])){
        $logger->error("{$error['type']} {$error['message']} {$error['file']} {$error['line']}");
    }
});



//Set mailer
$transport = Swift_SmtpTransport::newInstance(MAIL_SMTP_SERVER, 465, 'ssl');
$transport->setUsername(MAIL_USER);
$transport->setPassword(MAIL_PWD);
$mailer = Swift_Mailer::newInstance($transport);

//Set cURL for ElasticSearch

try{

    $elastic = new Elasticsearch(CURL_ELASTIC_CONNECTION);

} catch (Exception $e) {
    $logger->error($e->getMessage());
    die("ElasticSearch error: {$e->getMessage()}");
}


//Set mongo
try {
    
    $options = array(
        "readPreference" => MongoClient::RP_PRIMARY_PREFERRED
        );
        
    $mongo = new MongoClient(MDB_CONN_STRING, $options);
}
catch (MongoConnectionException $e) {
    $logger->error("Error al intentar conectar a mongo");
    $logger->error($e->getMessage());
    $logger->error(MDB_CONN_STRING);
    die("MongoDB error: {$e->getMessage()}");
} catch (Exception $e) {
    $logger->error($e->getMessage());
    die("MongoDB error: {$e->getMessage()}");
}

//Set Mysql and models
// Use Loader() to autoload our models
$loader = new \Phalcon\Loader();

$loader->registerDirs(array(
    __DIR__ . '/models/'
))->register();

//Set up the database service, values set in config.php
$di->set('db', function(){
    return new Phalcon\Db\Adapter\Pdo\Mysql(array(
        "host"      => MYSQL_HOST,
        "username"  => MYSQL_USER,
        "password"  => MYSQL_PASS,
        "dbname"    => MYSQL_DB_NAME
    ));
});



/**
 * Default routes
 */
$app->get("/", function () use($app, $mongo) {

    $mdb = $mongo->selectDB(MDB_DB_NAME);
    $books = $mdb->selectCollection(MDB_COLLECTION);

    $json = array(
        "version" => VERSION,
        "mongodb" => 'ok',
        "collection" => $books->getName(),
        "db" => "ok",
        "routes" => array()
    );

    foreach($app->router->getRoutes() as $route){
        $json["routes"][] = $route->getPattern();
    }

    echo "<pre>";
    echo json_encode($json, JSON_PRETTY_PRINT + JSON_UNESCAPED_SLASHES);
    echo "</pre>";
});

$app->notFound(function () use ($app) {
    $app->response->setStatusCode(404, "Not Found")->sendHeaders();
    echo 'La url solicitada no existe!';
});

//Before middleware
$app->before(function () use ($app, $logger) {

    $logger->log("{$app->request->getScheme()} {$app->request->getHttpHost()} {$app->request->getMethod()} {$app->request->get("_url")}");

});

/**
 * Route handlers 5 total
 */

require("handlers/import.php");
require("handlers/books.php");
require("handlers/reports.php");
require("handlers/prices.php");
require("handlers/elastic.php");


//Handle request
$app->handle();



