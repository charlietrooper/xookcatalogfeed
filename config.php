<?php

/**
 * App config
 *
 */


//Variable de la versión del programa
define("VERSION", "0.0.1");
//Variable de ruta de los logs del programa con configuración de fecha y nombre
define("LOG_FILE", "logs/catalog_" . date("Ymd") . ".log");
//Variable de la dirección para el servidor FTP para la importación y actualización del catálogo

/**
 * Kobo FTP config
 *
 */

define("FTP_ADDRESS", "ftp.kobobooks.com");
define("FTP_PORT", "22");
//Variable de usuario para el servidor FTP para la importación y actualización del catálogo
define("FTP_USER", "orbil");
//Variable del password para el servidor FTP para la importación y actualización del catálogo
define("FTP_PASS", "FbzTHzPD");
//Feed files directory
define("FTP_FEEDS_DIR", '/feeds/');


/**
 * Data processing config
 *
 */
define("DATA_OUTPUT_DIR", 'data/output/');
define("DATA_PENDING_DIR", 'data/pending/');
define("DATA_PROCESSED_DIR", 'data/processed/');
//Variable de ruta local para datos del programa
define("DATA_DIR", 'data/');
define("FTP_LOG_FILE", 'data/processed/readme');
define("MONGO_SCRIPTS_LOCATION",'scripts/');
define("DATA_REPORTS_DIR",'/var/www/html/XookCatalogFeed/data/reports/');

/**
 * Mongodb config
 *
 */
//Variable de usuario para el servidor Mongo para la importación y actualización del catálogo
define("MDB_USER", "orbile_production");
//Variable del password para el servidor Mongo para la importación y actualización del catálogo
define("MDB_PASS", "palmas123+");
//Variable del puerto para el servidor Mongo para la importación y actualización del catálogo
define("MDB_PORT", "53582");
//Variable de la base de datos a usar en Mongo
define("MDB_DB_NAME", "orbile_production");
//Variable de las colecciónes de datos a usar en Mongo
define("MDB_COLLECTION", "books");
define("MDB_COLLECTION_TODAYPRICES", "today_prices");
define("MDB_COLLECTION_FUTUREPRICES", "future_prices_compare");
define("MDB_COLLECTION_IMPRINTS", "conteo_imprints");
define("MDB_COLLECTION_PUBLISHERS", "conteo_publishers");
define("MDB_COLLECTION_ACCOUNT_HOLDER_NAME", "conteo_accountholderid");
define("MDB_COLLECTION_LANGUAGE","conteo_lenguaje");
define("MDB_COLLECTION_FREBOOKS", "freebooks");


//Nombre del replica se
define('MDB_REPLICA_SET_NAME', 'rs-ds053582');

define('MDB_HOST', 'ds053582-a0.mongolab.com:53582,ds053582-a1.mongolab.com:53582');


//Variable de ruta para las herramientas de Mongo
define("MDB_BIN_DIR", '/usr/bin/');

//String de conexion
define("MDB_CONN_STRING", 'mongodb://'.MDB_USER.':'.MDB_PASS.'@ds053582-a0.mongolab.com:53582,ds053582-a1.mongolab.com:53582/'.MDB_DB_NAME );


/**
 * MongoDB commands
 */
define('MONGO_IMPORT_CMD', MDB_BIN_DIR . 'mongoimport ');
define("MONGO_IMPORT", MONGO_IMPORT_CMD . ' --upsert  --host '.MDB_REPLICA_SET_NAME .'/'. MDB_HOST . ' --username ' . MDB_USER . ' --password ' . MDB_PASS . ' --db ' . MDB_DB_NAME . ' --collection ' . MDB_COLLECTION . ' --file ' . DATA_OUTPUT_DIR);
define("MONGO_SCRIPT_CMD",MDB_BIN_DIR. 'mongo ');
define("MONGO_SCRIPT", MONGO_SCRIPT_CMD . 'ds053582-a0.mongolab.com:53582/'.MDB_DB_NAME. ' -u '.MDB_USER. ' -p '. MDB_PASS);
define('MONGO_EXPORT_CMD', MDB_BIN_DIR . 'sudo mongoexport ');
define("MONGO_EXPORT", MONGO_EXPORT_CMD . ' --type=csv  --host ds053582-a0.mongolab.com'. ' --port '. MDB_PORT . ' --username ' . MDB_USER . ' --password ' . MDB_PASS);


/**
 * MYSQL DB config
 */
define("MYSQL_USER", "root");
define("MYSQL_PASS", "palmas123+");
define("MYSQL_HOST", "localhost");
define("MYSQL_DB_NAME", "xookdb");


/**
 * Email service config
 *
 */
define("MAIL_SMTP_SERVER", "smtp.gmail.com");
define("MAIL_USER", "luismirandadiaz@gmail.com");
define("MAIL_PWD", "Spiderman_0021");
define("MAIL_ADDS", serialize(array(
    "luismirandadiaz@gmail.com" => "Troopers"
)));


/**
 * PHP Error reporting config
 */
error_reporting(E_ALL);
ini_set('display_errors', 1);
set_time_limit ( 0 );




