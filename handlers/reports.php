<?php
/*
 * Created by PhpStorm.
 * User: CarlosMiranda
 * Date: 03/12/2015
 * Time: 9:03
 */


$app->get('/reports/futureprices/load', function () use ($app, $logger) {
    $future_price_cmd = MONGO_SCRIPT . ' '.MONGO_SCRIPTS_LOCATION.'future_prices.js';
    $logger->log("Executing command: $future_price_cmd ...");
    exec($future_price_cmd, $output, $return);

});

$app->get('/reports/todayprices/load', function () use ($app, $logger) {
    $today_price_cmd = MONGO_SCRIPT . ' '.MONGO_SCRIPTS_LOCATION.'price_update.js';
    $logger->log("Executing command: $today_price_cmd ...");
    exec($today_price_cmd, $output, $return);
});

$app->get('/reports/compareprices/load', function () use ($app, $logger) {
    $compare_price_cmd = MONGO_SCRIPT . ' '.MONGO_SCRIPTS_LOCATION.'compareFuture.js';
    $logger->log("Executing command: $compare_price_cmd ...");
    exec($compare_price_cmd, $output, $return);
});

/*
 * Llamada desde la ruta /imprints/load donde creara una lista de los imprints disponibles
 */
$app->get('/reports/imprints/load', function () use ($app, $logger, $mongo) {
    $imprint_cmd = MONGO_SCRIPT . ' '.MONGO_SCRIPTS_LOCATION.'imprints_update.js';
    $logger->log("Executing command: $imprint_cmd ...");
    exec($imprint_cmd, $output, $return);
});

/*Hola
 * Llamada desde la ruta /publishers/load donde creara una lista de los publishers disponibles
 */
$app->get('/reports/publishers/load', function () use ($app, $logger, $mongo) {
    $publisher_cmd = MONGO_SCRIPT . ' '.MONGO_SCRIPTS_LOCATION.'publishers_update.js';
    $logger->log("Executing command: $publisher_cmd ...");
    exec($publisher_cmd, $output, $return);
});

/*
 * Llamada desde la ruta /accountholderid/load donde creara una lista de los Account Holder ID disponibles
 */
$app->get('/reports/accountholderid/load', function () use ($app, $logger, $mongo) {
    $accholdid_cmd = MONGO_SCRIPT . ' '.MONGO_SCRIPTS_LOCATION.'accountholderid_update.js';
    $logger->log("Executing command:  $accholdid_cmd ...");
    exec($accholdid_cmd, $output, $return);
});

/*
 * Llamada desde la ruta /language/load donde creara una lista de los Account Holder ID disponibles
 */
$app->get('/reports/language/load', function () use ($app, $logger, $mongo) {
    $accholdid_cmd = MONGO_SCRIPT . ' '.MONGO_SCRIPTS_LOCATION.'language.js';
    $logger->log("Executing command:  $accholdid_cmd ...");
    exec($accholdid_cmd, $output, $return);
});

