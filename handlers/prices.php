<?php
MongoCursor::$timeout = -1;
$app->get('/todayprices/list/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_TODAYPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    //Aplicación del filtro hacia la colección
    $cursor = $collection->find()->sort(array("_id" => -1));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/todayprices/spanish/list', function () use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_TODAYPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    //Aplicación del filtro hacia la colección
    $where = array('language' => array('$regex' => new MongoRegex("/es/i")));
    $cursor = $collection->find($where)->sort(array("_id" => -1));
    foreach ($cursor as $document) {
        echo json_encode($document/*, JSON_PRETTY_PRINT*/);

    }
});


$app->get('/futureprices/list/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_FUTUREPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    //Aplicación del filtro hacia la colección
    $cursor = $collection->find()->sort(array("_id" => -1));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/futureprices/search/title/{title}/{itemsperpage}/{page}', function ($title = "",$itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_FUTUREPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    //Aplicación del filtro hacia la colección
    $where = array('title' => array('$regex' => new MongoRegex("/$title/i")));
    $cursor = $collection->find($where)->sort(array("title" => 1));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/todayprices/search/title/{title}/{itemsperpage}/{page}', function ($title = "",$itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_TODAYPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    //Aplicación del filtro hacia la colección
    $where = array('title' => array('$regex' => new MongoRegex("/$title/i")));
    $cursor = $collection->find($where)->sort(array("title" => 1));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/todayprices/search/isbn/{isbn}', function ($isbn = "") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_TODAYPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    //Aplicación del filtro hacia la colección
    $filter = array("isbn" => $isbn);
    $cursor = $collection->find($filter);
    //Aplicación del limite hacia la colección
    echo '{'.'"Items":'.'{'.'"Item":'.json_encode(iterator_to_array($cursor, false),true).'}'.'}';
});

$app->get('/futureprices/search/isbn/{isbn}', function ($isbn = "") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_FUTUREPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    //Aplicación del filtro hacia la colección
    $filter = array("isbn" => $isbn);
    $cursor = $collection->find($filter);
    //Aplicación del limite hacia la colección
    echo '{'.'"Items":'.'{'.'"Item":'.json_encode(iterator_to_array($cursor, false),true).'}'.'}';
});

$app->get('/futureprices/future/sellingprice/50orless/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_FUTUREPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("future_selling_price" => array('$lte' => 50)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/futureprices/today/sellingprice/50orless/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_FUTUREPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("today_selling_price" => array('$lte' => 50)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/todayprices/sellingprice/50orless/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_TODAYPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("sellingprice" => array('$lte' => 50)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/futureprices/future/listprice/50orless/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_FUTUREPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("future_list_price" => array('$lte' => 50)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/futureprices/today/listprice/50orless/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_FUTUREPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("today_list_price" => array('$lte' => 50)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/todayprices/listprice/50orless/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_TODAYPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("listprice" => array('$lte' => 50)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/futureprices/future/sellingprice/50to100/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_FUTUREPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("future_selling_price" => array('$gte' => 50,'$lte'=>100)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/futureprices/today/sellingprice/50to100/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_FUTUREPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("today_selling_price" => array('$gte' => 50,'$lte'=>100)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/todayprices/sellingprice/50to100/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_TODAYPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("sellingprice" => array('$gte' => 50,'$lte'=>100)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/futureprices/future/listprice/50to100/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_FUTUREPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("future_list_price" => array('$gte' => 50,'$lte'=>100)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/futureprices/today/listprice/50to100/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_FUTUREPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("today_list_price" => array('$gte' => 50,'$lte'=>100)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/todayprices/listprice/50to100/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_TODAYPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("listprice" => array('$gte' => 50,'$lte'=>100)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/futureprices/future/sellingprice/100to200/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_FUTUREPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("future_selling_price" => array('$gte' => 100,'$lte'=>200)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/futureprices/today/sellingprice/100to200/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_FUTUREPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("today_selling_price" => array('$gte' => 100,'$lte'=>200)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/todayprices/sellingprice/100to200/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_TODAYPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("sellingprice" => array('$gte' => 100,'$lte'=>200)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/futureprices/future/listprice/100to200/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_FUTUREPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("future_list_price" => array('$gte' => 100,'$lte'=>200)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/futureprices/today/listprice/100to200/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_FUTUREPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("today_list_price" => array('$gte' => 100,'$lte'=>200)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/todayprices/listprice/100to200/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_TODAYPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("listprice" => array('$gte' => 100,'$lte'=>200)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/futureprices/future/sellingprice/200to500/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_FUTUREPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("future_selling_price" => array('$gte' => 200,'$lte'=>500)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/futureprices/today/sellingprice/200to500/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_FUTUREPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("today_selling_price" => array('$gte' => 200,'$lte'=>500)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/todayprices/sellingprice/200to500/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_TODAYPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("sellingprice" => array('$gte' => 200,'$lte'=>500)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/futureprices/future/listprice/200to500/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_FUTUREPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("future_list_price" => array('$gte' => 200,'$lte'=>500)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/futureprices/today/listprice/200to500/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_FUTUREPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("today_list_price" => array('$gte' => 200,'$lte'=>500)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/todayprices/listprice/200to500/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_TODAYPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("listprice" => array('$gte' => 200,'$lte'=>500)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/futureprices/future/sellingprice/500ormore/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_FUTUREPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("future_selling_price" => array('$gte' => 500)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/futureprices/today/sellingprice/500ormore/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_FUTUREPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("today_selling_price" => array('$gte' => 500)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/todayprices/sellingprice/500ormore/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_TODAYPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("sellingprice" => array('$gte' => 500)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/futureprices/future/listprice/500ormore/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_FUTUREPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("future_list_price" => array('$gte' => 500)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/futureprices/today/listprice/500ormore/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_FUTUREPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("today_list_price" => array('$gte' => 500)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/todayprices/listprice/500ormore/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_TODAYPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("listprice" => array('$gte' => 500)));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/futureprices/future/sellingprice/sube/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_FUTUREPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("variacion" => "sube"));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/futureprices/future/sellingprice/baja/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_FUTUREPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("variacion" => "baja"));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});

$app->get('/futureprices/future/sellingprice/igual/{itemsperpage}/{page}', function ($itemsperpage = "",$page="") use ($app, $logger, $mongo) {
    //Inicio de la conexion a Mongo con la base de datos
    $db = (MDB_DB_NAME);
    $col = (MDB_COLLECTION_FUTUREPRICES);
    $mongo_db = $mongo->selectDB($db);
    //Inicio de la conexion de la coleccion dentro de la base de datos
    $collection = $mongo_db->$col;
    $cursor = $collection->find(array("variacion" => "igual"));
    if($page ==0){ echo "Página invalida"; }
    elseif($itemsperpage==0){echo "Número de items invalidos";}
    elseif($page ==1) {
        //Aplicación del limite hacia la colección
        $cursor->limit($itemsperpage);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);
    }
    else
    {
        $paginator = ($page - 1) * $itemsperpage;
        $cursor->limit($itemsperpage)->skip($paginator);
        $response = array("ItemsPerPage" => (int)$itemsperpage, "Page" => (int)$page, "TotalItems" => $cursor->count(), "Items" =>iterator_to_array($cursor, false));
        echo json_encode($response);

    }
});