<?php



$app->get('/search/elastic/find/term/title/{search}/{category}/{initialrange}/{finalrange}/{language}/{itemsperpage}/{page}', function ($search="",$category="",$initialrange="",$finalrange="",$language="",$page="",$itemsperpage="") use ($app, $logger, $mongo, $elastic) {

    $resultado = $elastic->titleTermSearch($search, $category, $initialrange, $finalrange,$language, $itemsperpage,$page);
    echo $resultado;
});

$app->get('/search/elastic/find/term/author/{search}/{category}/{initialrange}/{finalrange}/{language}/{itemsperpage}/{page}', function ($search="",$category="",$initialrange="",$finalrange="",$language="",$page="",$itemsperpage="") use ($app, $logger, $mongo, $elastic) {

    $resultado = $elastic->authorTermSearch($search, $category, $initialrange, $finalrange,$language, $itemsperpage,$page);
    echo $resultado;
});

$app->get('/search/elastic/find/match/title/{search}/{category}/{initialrange}/{finalrange}/{language}/{itemsperpage}/{page}', function ($search="",$category="",$initialrange="",$finalrange="",$language="",$page="",$itemsperpage="") use ($app, $logger, $mongo, $elastic) {
    $resultado = $elastic->titleMatchSearch($search, $category, $initialrange, $finalrange,$language, $itemsperpage,$page);
    echo $resultado;
});


$app->get('/search/elastic/find/match/author/{search}/{category}/{initialrange}/{finalrange}/{language}/{itemsperpage}/{page}', function ($search="",$category="",$initialrange="",$finalrange="",$language="",$page="",$itemsperpage="") use ($app, $logger, $mongo, $elastic) {
    $resultado = $elastic->authorMatchSearch($search, $category, $initialrange, $finalrange,$language, $itemsperpage,$page);
    echo $resultado;
});









