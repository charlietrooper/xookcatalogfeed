<?php
/**
 *
 * Handler for the import/** routes
 *
 * Created by PhpStorm.
 * User: AppStudio
 * Date: 15/05/2015
 * Time: 08:18 PM
 */

set_include_path(get_include_path() . PATH_SEPARATOR . 'lib/phpseclib');

use Prewk\XmlStringStreamer;
use Prewk\XmlStringStreamer\Parser\StringWalker;
use Prewk\XmlStringStreamer\Stream\File;

include('lib/phpseclib/Net/SSH2.php');
include('lib/phpseclib/Net/SFTP.php');
include('lib/Array2XML/Array2XML.php');
include('lib/Array2XML/XML2Array.php');

/**
 * Procesa un solo archivo {filename}.xml que debe estar presente en data/pending.
 * Lo tranforma en json y lo carga en mongodb
 * Usado para cargas completas manuales
 *
 */
$app->get('/import/process/pending/{filename}', function ($filename) use ($app, $logger, $transport, $mailer) {

    //Number of processed nodes (books)
    $count = 0;

    //Nombre del archivo local
    $local_file = DATA_PENDING_DIR . $filename;

    //Nombre del archivo final
    $json_file = DATA_OUTPUT_DIR . "$filename.json";

    //Metodo para la obtención del tamaño del archivo XML local
    $totalSize = filesize($local_file);

    $start_timestamp = date('Y-m-d H:i:s');

    // Se prepara el streaming y monitoreo con 16kb de buffer
    $progress = 0;
    $last_progress = 0;
    $stream = new File($local_file, 16384, function ($chunk, $readBytes) use ($progress, &$last_progress, $totalSize, $logger) {

        $progress = $readBytes / $totalSize;

        //report every 10%
        if ($progress >= $last_progress + 0.1) {
            $logger->log("Progress: $progress");
            $last_progress = $last_progress + 0.1;
        }
    });

    $start_timestamp = date('Y-m-d H:i:s');
    //Configura el parser

    $parser = new StringWalker;

    //Configura el streamer
    $streamer = new XmlStringStreamer($parser, $stream);

    //Creación del archivo final
    $file = fopen($json_file, "w") or die(json_encode("Could not open {$json_file} for writing"));

    $logger->log("Convirtiendo {$local_file} a {$json_file}...");
    //Procesamiento de nodos
    while ($node = $streamer->getNode()) {

        //Set json string ready for mongo insertion
        $json_string = Utils::getBookJSONFromXMLNode2($node);

        //Inserta la cadena en el archivo final
        fputs($file, $json_string . PHP_EOL);

        $count++;
    }

    //Cierra la edición del archivo final
    fclose($file);

    //Elimina la cache del proceso
    clearstatcache();

    $logger->log("{$json_file} creado.");


    //Ejecuta la herramienta de importación por medio de comando usando el archivo final
    /*$full_mongo_import_cmd = MONGO_IMPORT . $filename . '.json';
    $logger->log("Ejecutando: $full_mongo_import_cmd ...");
    exec($full_mongo_import_cmd, $output, $return);

    //Inicia secuencia de condicion con el valor de regreso de la función para analizar la importación
    if (!$return) {
        //Reporta en el log si se realizó la importación completa
        $logger->log("Importación a Mongo completa");
        $message = Swift_Message::newInstance();
        $message->setTo(unserialize(MAIL_ADDS));
        $message->setSubject("Importación a Mongo completa");
        $message->setBody("Sin problemas la importación en Mongo");
        $message->setFrom(MAIL_USER, "Carlos Miranda");
        $mailer->send($message);

    } else {
        //Reporta en el log si falla la importación de Mongo
        $logger->error("Falla en la importación de Mongo");
        $message = Swift_Message::newInstance();
        $message->setTo(unserialize(MAIL_ADDS));
        $message->setSubject("Falla en la importación de Mongo");
        $message->setBody("Consulta el error en la bitacora de movimientos");
        $message->setFrom(MAIL_USER, "Carlos Miranda");
        $mailer->send($message);
    }
    //Termina el proceso del archivo y lo reporta en el log
    $logger->log("Resultado almacenado en $json_file");

    $end_timestap = date('Y-m-d H:i:s');

    $catalog_feed_file = new CatalogFeedFile();
    $catalog_feed_file->file_path = $filename;
    $catalog_feed_file->server_path = $local_file;
    $catalog_feed_file->start_time = $start_timestamp;
    $catalog_feed_file->end_time = $end_timestap;
    $catalog_feed_file->num_ebooks = $count;

    if ($catalog_feed_file->save()) {
        $logger->log("{$catalog_feed_file->file_path} saved successfully");
    } else {
        $logger->error("{$catalog_feed_file->file_path} could not be saved");
    }

    //Send response*/
    $app->response->setContentType('application/json', 'UTF-8');
    die(json_encode("$json_file cargado a mongo"));


});


/**
 * Procesa todos los archivos xml presentes en data/pending
 * Los transforma en json y los pasa a la carpeta data/output y despues los inserta usando mongoimport
 */
$app->get('/import/process/pending/all', function () use ($app, $logger, $transport, $mailer) {

    $pending_files = glob(DATA_PENDING_DIR . '*.xml');

    $num_pending_files = count($pending_files);
    if ($num_pending_files === 0) {
        $logger->log("No files pending. Terminating...");
    }

    $logger->log("Processing pending files... ");

    //Glob already lists in ascending alphabetical-numerical order.\
    $curr_file = 0;
    $inactive_ebooks = 0;
    $active_ebooks = 0;
    foreach ($pending_files as $file) {

        //Get only the file name, strip path
        $file_name = basename($file);

        //Current file counter
        $curr_file++;

        //Start TS
        $start_timestamp = date('Y-m-d H:i:s');

        $logger->log("Pending file $curr_file/$num_pending_files: $file");

        /**
         * Check the file, skip  if something does not match the criteria
         */

        //Check daily delta feed
        if (strpos($file, 'Daily') === false) {
            $logger->log("$file is not a daily delta feed. [skip]");
            continue;
        }

        //Check if the file has already been processed
        $catalog_feed_file = CatalogFeedFile::findFirst(
            array("file_path=:file_path:",
                "bind" => array("file_path" => $file_name)
            )
        );

        if ($catalog_feed_file !== false) {
            $logger->error("$file_name already processed at: {$catalog_feed_file->start_time} [skip]");
            continue;
        } else {
            $logger->log("$file_name has not been processed before.");
        }


        //Converts xml to json file and puts it in the data/output dir
        $count = Utils::convertPendingXMLtoJSON($file_name, $logger);

        //Check count
        if ($count == 0) {
            $logger->error("$file was empty or could not parse, [skip]");
            continue;
        }


        $json_file = DATA_OUTPUT_DIR . "$file_name.json";
        $logger->log("$json_file created");

        ///Gather stats about the converted file
        $logger->log("Counting books...");

        $handle = fopen(DATA_OUTPUT_DIR . "$file_name.json", "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                // process the line read.
                if (strpos($line, "deprecatedRevision") === true) {
                    $inactive_ebooks++;
                } else {
                    $active_ebooks++;
                }
            }

            $logger->error("active: $active_ebooks inactive:$inactive_ebooks");

            fclose($handle);
        } else {
            // error opening the file.
            $logger->error("$handle was empty or could not count, [skip]");
        }

        //Mongo upsert
        $logger->log("Importing into mongo...");

        //Executes mongoimport command
        $full_mongo_import_cmd = MONGO_IMPORT . $file_name . '.json';
        $logger->log("Executing command: $full_mongo_import_cmd ...");
        exec($full_mongo_import_cmd, $output, $return);

        //Check if upsert was successful
        if (!$return) {
            //end timestamp
            $end_timestap = date('Y-m-d H:i:s');

            //Reporta en el log si se realizó la importación completa
            $logger->log("$file_name import successful");

            //Save import entry
            $logger->log("Saving import entry..");
            $catalog_feed_file = new CatalogFeedFile();
            $catalog_feed_file->file_path = $file_name;
            $catalog_feed_file->server_path = $file;
            $catalog_feed_file->start_time = $start_timestamp;
            $catalog_feed_file->end_time = $end_timestap;
            $catalog_feed_file->num_ebooks = $count;
            $catalog_feed_file->active_ebooks = $active_ebooks;
            $catalog_feed_file->$inactive_ebooks = $inactive_ebooks;

            if ($catalog_feed_file->save()) {
                $logger->log("{$catalog_feed_file->file_path} saved successfully");
            } else {
                $logger->error("{$catalog_feed_file->file_path} could not be saved");
            }


        } else {
            //Reporta en el log si falla la importación de Mongo
            $logger->error("$file_name import failed");
        }

        $inactive_ebooks = 0;
        $active_ebooks = 0;

    }//foreach files end

    $logger->log("Processed $curr_file/$num_pending_files files.");

});

/**
 * Handler para la importación de libros desde un archivo XML que se obtiene de un servidor FTP
 */
$app->get('/import/ftp/pending', function () use ($app, $logger, $transport, $mailer) {

    //Inicio del log con el nombre del archivo que se va a importar del servidor FTP
    $logger->log("Initializing FTP connection...");
    $sftp = new Net_SFTP(FTP_ADDRESS);
    $logger->log("Connecting to FTP server " . FTP_ADDRESS . "...");
    if (!$sftp->login(FTP_USER, FTP_PASS)) {
        $logger->error('Login Failed');
        exit('Login Failed');
    }

    $logger->log("Log-in sucessful.");

    $sftp->chdir('feeds');

    $logger->log("Iniciando descarga de archivos en repositorio feeds: ");

    //Download each book
    foreach ($sftp->nlist() as $raw) {
        $logger->log("Descargando archivo  $raw");
        $sftp->get($raw, DATA_PENDING_DIR . $raw);
        $logger->log("Descarga finalizada del archivo  $raw");
    }

    $logger->log("Proceso  de descarga terminado: ");
    $pen_files = preg_grep('/^([^.])/', scandir(DATA_PENDING_DIR));
    $logger->log("Iniciando proceso de extracción de archivos en repositorio local... ");

    foreach ($pen_files as $gzfile) {
        $extension = pathinfo($gzfile, PATHINFO_EXTENSION);
        if ($extension == 'gz') {
            $logger->log("Procesando archivo " . $gzfile . "Tiempo de inicio: " . date('Y-m-d H:i:s'));
            $buffer_size = 4096; // read 4kb at a time
            $out_file_name = str_replace('.gz', '', DATA_PENDING_DIR . $gzfile);
// Open our files (in binary mode)
            $file = gzopen(DATA_PENDING_DIR . $gzfile, 'rb');
            $out_file = fopen($out_file_name, 'wb');
// Keep repeating until the end of the input file
            while (!gzeof($file)) {
// Read buffer-size bytes
// Both fwrite and gzread and binary-safe
                fwrite($out_file, gzread($file, $buffer_size));
            }
// Files are done, close files
            fclose($out_file);
            gzclose($file);
            $logger->log("Proceso finalizada del archivo " . $gzfile . "Tiempo final: " . date('Y-m-d H:i:s'));
            rename(DATA_PENDING_DIR . $gzfile, DATA_PROCESSED_DIR . $gzfile);
            $logger->log("Archivo: " . $gzfile . " fue cambiado a la carpeta de Processed");
        } else {
            $logger->log("No hay archivos nuevos que procesar");
            die("No hay archivos nuevos que procesar");
        }
    }
});

/* Procesa todos los archivos xml presentes en data/output
* Los transforma en xml y los deja a la carpeta data/output
*/
$app->get('/import/jsonxml/multiple', function () use ($app, $logger) {
    $logger->log("Starting import method");

    $pending_files = glob(DATA_OUTPUT_DIR . '*.json');

    $num_pending_files = count($pending_files);
    if ($num_pending_files === 0) {
        $logger->log("No files pending. Terminating...");
    }

    $logger->log("Processing pending files... ");

    //Glob already lists in ascending alphabetical-numerical order.\
    $curr_file = 0;
    foreach ($pending_files as $file) {

        //Get only the file name, strip path
        $file_name = basename($file);
        /**
         * Check the file, skip  if something does not match the criteria
         */

        if (file_exists($file_name . ".xml")) {
            $logger->error("$file_name already processed [skip]");
            continue;
        }

        $xml_file = DATA_OUTPUT_DIR . $file_name . ".xml";
        $handle = fopen($file_name, "r");
        $ofile = fopen($xml_file, "w") or die("Could not open {$xml_file} for writing");
        fwrite($ofile, '<?xml version="1.0" encoding="utf-8"?>');
        //Current file counter
        $curr_file++;

        //Start TS
        $start_timestamp = date('Y-m-d H:i:s');

        $logger->log("Pending file $curr_file/$num_pending_files: $file");


        //Converts json to xml  file and puts it in the data/output dir
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $xmlstring = json_decode($line, true);
                if ($xmlstring == null) {
                    $logger->error(json_last_error_msg());
                    $logger->error("$file was empty or could not parse, [skip]");
                    continue;
                }
                unset($xmlstring['lastUpdate']);
                $xml = Array2XML::createXML('book', $xmlstring);
                fputs($ofile, $xml->saveXML($xml->documentElement));
            }
            fclose($ofile);
            fclose($handle);
        } else {
            $logger->error("Falla en la apertura del archivo");
        }


        $logger->log("$xml_file created");

    }//foreach files end

    $logger->log("Processed $curr_file/$num_pending_files files.");

});

$app->get('/import/jsonxml/single/{filename}', function ($filename) use ($app, $logger) {
    $logger->log("Starting import method");
    $xml_file = DATA_OUTPUT_DIR . $filename . ".xml";
    $handle = fopen(DATA_OUTPUT_DIR . $filename, "r");
    $file = fopen($xml_file, "w") or die("Could not open {$xml_file} for writing");
    fwrite($file, '<?xml version="1.0" encoding="utf-8"?>');
    if ($handle) {
        while (($line = fgets($handle)) !== false) {
            $xmlstring = json_decode($line, true);
            if ($xmlstring == null) {
                $logger->error(json_last_error_msg());
            }
            unset($xmlstring['lastUpdate']);
            $xml = Array2XML::createXML('book', $xmlstring);
            fputs($file, $xml->saveXML($xml->documentElement));
        }
        fclose($file);
        fclose($handle);
    } else {
        $logger->error("Falla en la apertura del archivo");
    }
});

$app->get('/import/reports/ftp', function () use ($app, $logger) {
    $f = fopen('data/processed/log', "r");
    if ($f) {
        while (($line = fgets($f)) !== false) {
            $ftplog = explode(' ', $line);
            switch ($ftplog[13]) {
                case "porruaftp":
                    $partner = "Porrua";
                    echo $partner . "\n";
                    break;
                case "gandhiftp":
                    $partner = "Gandhi";
                    echo $partner . "\n";
                    break;
                case "catalogo":
                    $partner = "Catalogo";
                    echo $partner . "\n";
                    break;
                default:
                    $partner = "Desconocido";
                    echo $partner . "\n";
                    break;
            }
            $date = $ftplog[2] . "-" . $ftplog[1] . "-" . $ftplog[4];
            $datecomplete = date_format(date_create_from_format('j-M-Y', $date), 'Y-m-d') . " " . $ftplog[3];
            echo $datecomplete . "\n";
            $delta = basename($ftplog[8], ".json");
            echo $delta . "\n";
            $partner_catalog_ftp_log = new PartnerCatalogFTPLog();
            $partner_catalog_ftp_log->download_time = $datecomplete;
            $partner_catalog_ftp_log->partner_name = $partner;
            $partner_catalog_ftp_log->delta_path = $delta;
            if ($partner_catalog_ftp_log->save() == false) {
                foreach ($partner_catalog_ftp_log->getMessages() as $message) {
                    echo $message;
                }
                echo("{ $partner_catalog_ftp_log->delta_path} of {$partner_catalog_ftp_log->partner_name} could not be saved");
            } else {
                echo("{ $partner_catalog_ftp_log->delta_path} of {$partner_catalog_ftp_log->partner_name} saved successfully");
            }
        }
    } else {
        echo("Falla en la apertura del archivo");
    }
    fclose($f);
});



