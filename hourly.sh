#!/bin/bash

# This script should be run as a sron script hourly.
# It is in charge of fetching Kobo deltas and updating the mongo db collection

#sync FTP
lftp sftp://orbil:FbzTHzPD@ftp.kobobooks.com:22 -e 'mirror --verbose --use-pget-n=8 -c /feeds /var/www/html/XookCatalogFeed/data/feeds
exit'

#uncompress if needed
yes n | gunzip -k /var/www/html/XookCatalogFeed/data/feeds/*Daily.xml.gz

#move to pending
mv /var/www/html/XookCatalogFeed/data/feeds/*Daily.xml /var/www/html/XookCatalogFeed/data/pending/

#Insert into mongo
wget -q -T0 http://catalog.orbiletest.com/import/process/pending/all