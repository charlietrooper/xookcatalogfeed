# XookCatalogFeed
Aplicación de carga y mantenimiento de catálogo

CRON update
lftp sftp://orbil:FbzTHzPD@ftp.kobobooks.com:22 -e 'mirror --verbose --use-pget-n=8 -c /feeds /var/www/html/XookCatalogFeed/data/feeds && exit' && gunzip /var/www/html/XookCatalogFeed/data/feeds/*Daily.xml.gz && mv /var/www/html/XookCatalogFeed/data/feeds/*Daily.xml /var/www/html/XookCatalogFeed/data/pending/ && wget -q -T0 http://catalog.orbiletest.com/import/process/pending/all

Mongo testdb
To connect using the shell: mongo ds053382-a0.mongolab.com:53382/orbile_test -u -p To connect using a driver via the standard URI (what's this?): mongodb://:@ds053382-a0.mongolab.com:53382,ds053382-a1.mongolab.com:53382/orbile_test?replicaSet=rs-ds053382

Import database mongorestore -h ds053382-a0.mongolab.com:53382 -d orbile_test -u -p Export database mongodump -h ds053382-a0.mongolab.com:53382 -d orbile_test -u -p -o Import collection mongorestore -h ds053382-a0.mongolab.com:53382 -d orbile_test -u -p <input .bson file> Export collection mongodump -h ds053382-a0.mongolab.com:53382 -d orbile_test -c -u -p -o JSON

Import collection mongoimport -h ds053382-a0.mongolab.com:53382 -d orbile_test -c -u -p --file Export collection mongoexport -h ds053382-a0.mongolab.com:53382 -d orbile_test -c -u -p -o CSV

Import collection mongoimport -h ds053382-a0.mongolab.com:53382 -d orbile_test -c -u -p --file <input .csv file> --type csv --headerline Export collection mongoexport -h ds053382-a0.mongolab.com:53382 -d orbile_test -c -u -p -o <output .csv file> --csv -f


Mongolab sandbox
mongodb://<dbuser>:<dbpassword>@ds043962.mongolab.com:43962/orbile_sandbox

user: orbile_sandbox
pass: palmas123+
Import / Export Helper

MongoDB provides two mechanisms for importing and exporting data. One way is via the mongoimport and mongoexport utilities. These allow you to import and export JSON and CSV representations of your data. The other way is with mongorestore and mongodump utilities which deal with binary dumps.

In this tab we provide pre-filled strings for the commands that we find most useful.

Copy and paste from below to import or export from this database. For a full list of options that can be used with these commands, please see MongoDB's documentation on this subject.

Binary

Import database

mongorestore -h ds043962.mongolab.com:43962 -d orbile_sandbox -u <user> -p <password> <input db directory>

Export database

mongodump -h ds043962.mongolab.com:43962 -d orbile_sandbox -u <user> -p <password> -o <output directory>

Import collection

mongorestore -h ds043962.mongolab.com:43962 -d orbile_sandbox -u <user> -p <password> <input .bson file>

Export collection

mongodump -h ds043962.mongolab.com:43962 -d orbile_sandbox -c <collection> -u <user> -p <password> -o <output directory> JSON

Import collection

mongoimport -h ds043962.mongolab.com:43962 -d orbile_sandbox -c <collection> -u <user> -p <password> --file <input file>

Export collection

mongoexport -h ds043962.mongolab.com:43962 -d orbile_sandbox -c <collection> -u <user> -p <password> -o <output file> CSV

Import collection

mongoimport -h ds043962.mongolab.com:43962 -d orbile_sandbox -c <collection> -u <user> -p <password> --file <input .csv file> --type csv --headerline

Export collection

mongoexport -h ds043962.mongolab.com:43962 -d orbile_sandbox -c <collection> -u <user> -p <password> -o <output .csv file> --csv -f <comma-separated list of field names>


