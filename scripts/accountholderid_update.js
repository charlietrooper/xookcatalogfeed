/**
 * Created by CarlosMiranda on 08/03/2016.
 */
/***** CONFIG ****/
var query = "$accountHolderId";


//set to 0 for no limit
var limit = 3000000;

var collection = "conteo_accountholderid";

/********************/

var start_time = new Date().getTime();


/*Limpiamos la colección*/

db.collection.remove({});

db.books.aggregate([ { $limit: limit },{ $group: { _id: query , count: { $sum: 1 }}},{ $out : collection }]);

var end_time = new Date().getTime();
print('Elapsed: ', (end_time - start_time) / 1000, ' s');