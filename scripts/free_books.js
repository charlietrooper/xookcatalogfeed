/********************

 Script para calcular los precios al dia de cada uno de los ebooks en la colección "books" e insertarlos en la coleccion "today_prices"

 Para probar:
 1. Conectate al servider de orbiletest con putty
 2. Actualiza el contenido de /home/ubuntu/price_update.js
 3. Conectarse a mongo mongo ds053582-a0.mongolab.com:53582/orbile_production -u orbile_production -p
 4. En la consola de mongo escribir load("free_books.js")

 Antes de correr el proceso sin el limit(5) asegurarse de comentar todos los mensajes debug: //print y //printjson
 *******************/
/***** CONFIG ****/
var query = {
    "activeRevision.@attributes.id": {
        "$exists": true
    }
};

var hint = {
    "activeRevision.@attributes.id": 1
};

//set to 0 for no limit
var limit = 0;

//BISAC Mapping

var bisac = [];
bisac['ANT'] = 'Antigüedades y coleccionables';
bisac['ARC'] = 'Arquitectura';
bisac['ART'] = 'Arte';
bisac['PER'] = 'Artes escénicas';
bisac['SEL'] = 'Autoayuda y desarrollo personal';
bisac['STU'] = 'Auxiliares de estudio';
bisac['BIO'] = 'Biografía y autobiografía';
bisac['SOC'] = 'Ciencias sociales';
bisac['SCI'] = 'Ciencias';
bisac['CKB'] = 'Cocina';
bisac['LCO'] = 'Colecciónes literarias';
bisac['CGN'] = 'Comics y novelas gráficas';
bisac['COM'] = 'Computación';
bisac['TRU'] = 'Crimen y misterio';
bisac['LIT'] = 'Crítica e historia literaria';
bisac['DES'] = 'Diseño';
bisac['EDU'] = 'Educación';
bisac['FAM'] = 'Familia';
bisac['FIC'] = 'Ficción';
bisac['PHI'] = 'Filosofía';
bisac['PHO'] = 'Fotografía';
bisac['HIS'] = 'Historia';
bisac['HOM'] = 'Hogar y mantenimiento';
bisac['HUM'] = 'Humor';
bisac['FOR'] = 'Idiomas';
bisac['LAN'] = 'Idiomas';
bisac['GAR'] = 'Jardinería';
bisac['GAM'] = 'Juegos y videojuegos';
bisac['JUV'] = 'Juvenil: Ficción';
bisac['YAF'] = 'Juvenil: Ficción';
bisac['YAN'] = 'Juvenil: No ficción';
bisac['JNF'] = 'Juvenil: No ficción';
bisac['LAW'] = 'Leyes';
bisac['CRA'] = 'Manualidades y pasatiempos';
bisac['PET'] = 'Mascotas';
bisac['MAT'] = 'Matemáticas';
bisac['MED'] = 'Medicina';
bisac['MUS'] = 'Música';
bisac['NAT'] = 'Naturaleza';
bisac['BUS'] = 'Negocios y economía';
bisac['POE'] = 'Poesía';
bisac['POL'] = 'Política y gobierno';
bisac['PSY'] = 'Psicología';
bisac['REF'] = 'Referencia';
bisac['BIB'] = 'Religión';
bisac['REL'] = 'Religión';
bisac['HEA'] = 'Salud y deportes';
bisac['OCC'] = 'Salud y deportes';
bisac['SPO'] = 'Salud y deportes';
bisac['DRA'] = 'Teatro';
bisac['TEC'] = 'Tecnología';
bisac['TRA'] = 'Transporte y logística';
bisac['TRV'] = 'Viajes';

//BIC Mapping

var bic = [];
bic['YB'] = 'Actividades y dibujo';
bic['TV'] = 'Agricultura y ganadería';
bic['WC'] = 'Antigüedades y coleccionables';
bic['DQ'] = 'Antologías';
bic['HD'] = 'Arqueología';
bic['AM'] = 'Arquitectura';
bic['AB'] = 'Arte';
bic['AF'] = 'Arte';
bic['AG'] = 'Arte';
bic['AK'] = 'Arte';
bic['YZ'] = 'Artículos de escritorio; infantiles y varios';
bic['PG'] = 'Astronomía; espacio y tiempo';
bic['YX'] = 'Asuntos personales y sociales';
bic['VS'] = 'Autoayuda y desarrollo personal';
bic['FJ'] = 'Aventura';
bic['UN'] = 'Bases de datos';
bic['BG'] = 'Biografía y autobiografía';
bic['BJ'] = 'Biografía y autobiografía';
bic['BK'] = 'Biografía y autobiografía';
bic['BM'] = 'Biografía y autobiografía';
bic['PS'] = 'Biología';
bic['FL'] = 'Ciencia ficción';
bic['PD'] = 'Ciencias';
bic['RB'] = 'Ciencias';
bic['AP'] = 'Cine; TV y radio';
bic['DB'] = 'Clásicos';
bic['WB'] = 'Cocina';
bic['FX'] = 'Comics y novelas gráficas';
bic['UK'] = 'Computación';
bic['UL'] = 'Computación';
bic['UM'] = 'Computación';
bic['UQ'] = 'Computación';
bic['UR'] = 'Computación';
bic['UY'] = 'Computación';
bic['FF'] = 'Crimen y misterio';
bic['DS'] = 'Crítica e historia literaria';
bic['AS'] = 'Danza';
bic['EB'] = 'Educación';
bic['EL'] = 'Educación';
bic['ES'] = 'Educación';
bic['JN'] = 'Educación';
bic['YQ'] = 'Educación';
bic['TJ'] = 'Electrónica y comunicaciones';
bic['MQ'] = 'Enfermería';
bic['FP'] = 'Erótica';
bic['UD'] = 'Estilo de vida';
bic['WJ'] = 'Estilo de vida';
bic['GT'] = 'Estudios interdisciplinarios';
bic['FM'] = 'Fantasía';
bic['BT'] = 'Ficción';
bic['FA'] = 'Ficción';
bic['FC'] = 'Ficción';
bic['FY'] = 'Ficción';
bic['FZ'] = 'Ficción';
bic['HP'] = 'Filosofía';
bic['KF'] = 'Finanzas y contabilidad';
bic['PH'] = 'Física';
bic['AJ'] = 'Fotografía';
bic['RG'] = 'Geografía';
bic['JW'] = 'Guerra';
bic['AC'] = 'Historia del arte';
bic['WN'] = 'Historía natural';
bic['HB'] = 'Historia';
bic['WK'] = 'Hogar y mantenimiento';
bic['FK'] = 'Horror y fantasmas';
bic['WH'] = 'Humor';
bic['CB'] = 'Idiomas';
bic['CJ'] = 'Idiomas';
bic['KN'] = 'Industria';
bic['YF'] = 'Infantil: Ficción';
bic['YN'] = 'Infantil: General';
bic['YD'] = 'Infantil: Poesía';
bic['TQ'] = 'Ingeniería ambiental';
bic['TC'] = 'Ingeniería bioquímica';
bic['TN'] = 'Ingeniería civil';
bic['TH'] = 'Ingeniería energética';
bic['TG'] = 'Ingeniería mecáncia';
bic['WQ'] = 'Interés local';
bic['GP'] = 'Investigación';
bic['WM'] = 'Jardinería';
bic['LA'] = 'Leyes';
bic['LB'] = 'Leyes';
bic['LN'] = 'Leyes';
bic['LR'] = 'Leyes';
bic['CF'] = 'Lingüística';
bic['WD'] = 'Manualidades y pasatiempos';
bic['WF'] = 'Manualidades y pasatiempos';
bic['PB'] = 'Matemáticas';
bic['MB'] = 'Medicina';
bic['MF'] = 'Medicina';
bic['MJ'] = 'Medicina';
bic['MM'] = 'Medicina';
bic['MN'] = 'Medicina';
bic['MR'] = 'Medicina';
bic['MX'] = 'Medicina';
bic['RN'] = 'Medio ambiente';
bic['UG'] = 'Medios gráficos y digitales';
bic['FQ'] = 'Mitos y leyendas';
bic['FT'] = 'Mitos y leyendas';
bic['GM'] = 'Museología';
bic['AV'] = 'Música';
bic['KC'] = 'Negocios y economía';
bic['KJ'] = 'Negocios y economía';
bic['UF'] = 'Negocios y economía';
bic['DN'] = 'No ficción';
bic['FV'] = 'Novela histórica';
bic['FW'] = 'Novela religiosa';
bic['WZ'] = 'Otros';
bic['RP'] = 'Planeación de areas y regiones';
bic['DC'] = 'Poesía';
bic['JP'] = 'Politica y gobierno';
bic['JM'] = 'Psicología';
bic['PN'] = 'Química';
bic['TD'] = 'Química';
bic['UT'] = 'Redes y comunicaciones';
bic['GB'] = 'Referencia';
bic['GL'] = 'Referencia';
bic['YR'] = 'Referencia: infantil';
bic['HR'] = 'Religión';
bic['FR'] = 'Romance';
bic['VF'] = 'Salud y deportes';
bic['VX'] = 'Salud y deportes';
bic['WS'] = 'Salud y deportes';
bic['JK'] = 'Servicio social';
bic['JF'] = 'Sociedad y cultura';
bic['JH'] = 'Sociología y antropología';
bic['AN'] = 'Teatro';
bic['DD'] = 'Teatro';
bic['TB'] = 'Tecnología';
bic['TT'] = 'Tecnología';
bic['UB'] = 'Tecnología';
bic['FH'] = 'Terror y suspenso';
bic['TR'] = 'Transporte y logística';
bic['WG'] = 'Transporte y logística';
bic['MZ'] = 'Veterinaria';
bic['WT'] = 'Viajes';

/********************/

/*Limpiamos la colección*/

db.freebooks.remove({});


// Revisa si el libro es gratuito y lo inserta con 0
// Si cae en este caso regresa true
function handleFree(data) {

    var priceDoc;
    var cat;
    var propValue;
    var propName;
    var cat_bisac;
    var cat_bic;

    if (data["activeRevision"]["prices"]["price"]["sellingprice"] == "0.00") {
        if (data['activeRevision']['categories']) {
            var categoria = data['activeRevision']['categories']['category'];

            if (categoria[0]) {
                if (categoria[0]['@attributes']['system'] == 'BISAC') {
                    cat_bisac = categoria[0]['@value'].substring(0, 3);
                    for (propName in bisac) {
                        propValue = bisac[propName]
                        if (propName == cat_bisac) {
                            //print(propValue);
                            cat = propValue;
                        }
                    }
                }
                else if (categoria[0]['@attributes']['system'] == 'BIC') {
                    cat_bic = categoria[0]['@value'].substring(0, 2);
                    for (propName in bic) {
                        propValue = bic[propName]
                        if (propName == cat_bic) {
                            //print(propValue);
                            cat = propValue;
                        }
                    }
                }
                else {
                    cat = categoria[0]['@value'];
                }
                priceDoc = {
                    '_id': data['_id'],
                    'active_revision_id': data['activeRevision']['@attributes']['id'],
                    'title': data['activeRevision']['title'],
                    'author': data['activeRevision']['contibutors'],
                    'language': data['activeRevision']['language'],
                    'isbn': data['isbn'],
                    'category': cat,
                    'listprice': parseFloat("0.00"),
                    'sellingprice': parseFloat("0.00"),
                    'cogs': parseFloat("0.00"),
                    'coverImage': data['activeRevision']['links']['coverImage'],
                    'itemPage': data['activeRevision']['links']['itemPage']
                };
            }
            else if (categoria['@attributes']['system']) {
                if (categoria['@attributes']['system'] == 'BISAC') {
                    cat_bisac = categoria['@value'].substring(0, 3);
                    for (propName in bisac) {
                        propValue = bisac[propName]
                        if (propName == cat_bisac) {
                            //print(propValue);
                            cat = propValue;
                        }
                    }
                }
                else if (categoria['@attributes']['system'] == 'BIC') {
                    cat_bic = categoria['@value'].substring(0, 2);
                    for (propName in bic) {
                        propValue = bic[propName]
                        if (propName == cat_bic) {
                            //print(propValue);
                            cat = propValue;
                        }
                    }
                }
                else {
                    cat = categoria['@value'];
                }

                priceDoc = {
                    '_id': data['_id'],
                    'active_revision_id': data['activeRevision']['@attributes']['id'],
                    'title': data['activeRevision']['title'],
                    'author': data['activeRevision']['contibutors'],
                    'language': data['activeRevision']['language'],
                    'isbn': data['isbn'],
                    'category': cat,
                    'listprice': parseFloat("0.00"),
                    'sellingprice': parseFloat("0.00"),
                    'cogs': parseFloat("0.00"),
                    'coverImage': data['activeRevision']['links']['coverImage'],
                    'itemPage': data['activeRevision']['links']['itemPage']
                };

            }
            else {
                priceDoc = {
                    '_id': -1,
                    'active_revision_id': data['activeRevision']['@attributes']['id'],
                    'title': -1,
                    'author': -1,
                    'language': -1,
                    'isbn': -1,
                    'category': -1,
                    'listprice': -1,
                    'sellingprice': -1,
                    'cogs': -1,
                    'coverImage': -1,
                    'itemPage': -1
                };
            }

        }
        else
        {
            priceDoc = {
                '_id': -1,
                'active_revision_id': data['activeRevision']['@attributes']['id'],
                'title': -1,
                'author': -1,
                'language': -1,
                'isbn': -1,
                'category': -1,
                'listprice': -1,
                'sellingprice': -1,
                'cogs': -1,
                'coverImage': -1,
                'itemPage': -1
            };
        }
        return priceDoc;
    }
    else {
        return false;
    }


}


//Start time for profiling
var start_time = new Date().getTime();

//Main update function
db.books.find(query).hint(hint).limit(limit).forEach(function (data) {

    var priceDoc;

    //Mensajes debug, hay que quitarlos cuando corramos el proceso complet
    //print("==Calculando precio para _id:" + data._id + " activeRevision.@attributes.id:" + data['activeRevision']['@attributes']['id'] + " isbn:" + data['isbn']+"==");


    //Maneja el caso gratuito.
    priceDoc = handleFree(data);
    if (priceDoc !== false) {
        db.freebooks.save(priceDoc);
    }


});

var end_time = new Date().getTime();
print('Elapsed: ', (end_time - start_time) / 1000, ' s');



