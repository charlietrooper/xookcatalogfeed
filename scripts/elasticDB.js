
/********************//*


 /*Limpiamos la colección*/

var start_time_es = new Date().getTime();

db.todaypricesespanol.remove({});

db.today_prices.aggregate([ { $match: {"language":/ES/i} }, { $out: "todaypricesespanol" } ]);

var end_time_es = new Date().getTime();
print('Elapsed: ', (end_time_es - start_time_es) / 1000, ' s');