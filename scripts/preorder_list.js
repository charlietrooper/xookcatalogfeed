/**
 * Created by CarlosMiranda on 30/03/2016.
 */

db.preorders.remove({});

var preorderDoc;
var i;
//set to 0 for no limit
var limit = 0;

data = db.books.find(
    {"activeRevision.availableForPreorder.from.@attributes.region":"MX"})
    .hint({"activeRevision.@attributes.id":1}).limit(limit).toArray();

for (i=0;i<data.length; i++ ) {

    preorderDoc =
    {
        'isbn': data[i]['isbn'],
        'title': data[i]['activeRevision']['title'],
        'active_revision_id': data[i]['activeRevision']['@attributes']['id'],
        'preorder_initial_date': data[i]['activeRevision']['prices']['price']['@attributes']['from'],
        'preorder_end_date': data[i]['activeRevision']['prices']['price']['@attributes']['to'],
        'preorder_listprice': data[i]['activeRevision']['prices']['price']['listprice']['@value'],
        'preorder_sellingprice': data[i]['activeRevision']['prices']['price']['sellingprice']['@value'],
        'preorder_language': data[i]['activeRevision']['language']
    };

    db.preorders.save(preorderDoc);
}