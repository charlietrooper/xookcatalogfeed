/**
 * Created by CharlieMiranda on 12/05/16.
 */
var query = "$activeRevision.language"


var limit = 2600000;

var collection = "conteo_language";

/********************/

var start_time = new Date().getTime();

/*Limpiamos la colección*/

db.collection.remove({});

db.books.aggregate([ { $limit: limit },{ $group: { _id: query , count: { $sum: 1 }}},{ $out : collection }]);

var end_time = new Date().getTime();
print('Elapsed: ', (end_time - start_time) / 1000, ' s');
