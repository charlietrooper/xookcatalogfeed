
/**** CONFIG ***/


//set to 0 for no limit
var limit = 0;
var variacion;

/******************/


/***Limpiamos la colección***/


db.future_prices_compare.remove({});

db.future_prices.find().limit(limit).forEach(
    function(doc) {
        data = db.today_prices.find({'active_revision_id': doc['active_revision_id']}).toArray();
        if(parseFloat(data[0]['sellingprice'])<parseFloat(doc['sellingprice']))
        {
            variacion = "sube";
        }
        else if(parseFloat(data[0]['sellingprice'])>parseFloat(doc['sellingprice']))
        {
            variacion = "baja";
        }
        else if(parseFloat(data[0]['sellingprice'])==parseFloat(doc['sellingprice']))
        {
            variacion = "igual";
        }
        priceDoc = {
            'active_revision_id': doc['active_revision_id'],
            'title': doc['title'],
            'isbn': doc['isbn'],
            'future_list_price':  parseFloat(doc['listprice']),
            'future_selling_price':  parseFloat(doc['sellingprice']),
            'future_price_from_date': doc['fromdate'],
            'future_ispreorder': doc['preorder'],
            'today_list_price':  parseFloat(data[0]['listprice']),
            'today_selling_price':  parseFloat(data[0]['sellingprice']),
            'variacion': variacion
        };
        db.future_prices_compare.save(priceDoc);
    }
);

