/*

 */
/***** CONFIG ****/


//set to 0 for no limit
var limit = 0;

/********************//*


 /*Limpiamos la colección*/

var start_time = new Date().getTime();

db.today_prices_es.remove({});

db.today_prices.aggregate([ { $match: {"language":/ES/i} }, { $out: "today_prices_es" } ]);

var end_time = new Date().getTime();
print('Elapsed: ', (end_time - start_time) / 1000, ' s');