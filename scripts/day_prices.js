/*****

 Script para calcular los precios al dia de cada uno de los ebooks en la colección "books" e insertarlos en la coleccion "day_prices"

 Para probar:
 1. Conectate al servider de orbiletest con putty
 2. Actualiza el contenido de /home/ubuntu/price_update.js
 3. Conectarse a mongo mongo ds053582-a0.mongolab.com:53582/orbile_production -u orbile_production -p
 4. En la consola de mongo escribir load("day_prices.js")

 Antes de correr el proceso sin el limit(5) asegurarse de comentar todos los mensajes debug: print y printjson
 *******************/
/***** CONFIG ****/
var query = {
    "activeRevision.@attributes.id": {
        "$exists": true
    }
};

var hint = {
    "activeRevision.@attributes.id": 1
};

//set to 0 for no limit
var limit = 10;

/********************/





// Revisa si el que no tiene llave numerica es el actual
// Si es el actual regresa un objeto
//Si no es regresa false
function handlePrice(data, price, currentDate) {

    //Todos tienen fromDate
    var fromDate;


    //Checamos si tiene fromDate
    if (typeof price["@attributes"]["from"] !== "undefined") {

        //Obtenemos el fromDate
        fromDate =  new Date(price["@attributes"]["from"]);

        //comparamos la fecha
        if (currentDate.getTime() == fromDate.getTime()) {

            print("Concuerda el día  del precio , insertando...");

            priceDoc = {
                'active_revision_id': data['activeRevision']['@attributes']['id'],
                'book_id': data['@attributes']['id'],
                'isbn': data['isbn'],
                'listprice': price["listprice"]["@value"],
                'sellingprice': price["sellingprice"]["@value"]
            };
            return priceDoc;
        } //end if
    } //end else

    return false;


}//end handlePrice

//Start time for profiling
var start_time = new Date().getTime();

//Main update function
db.books.find(query).hint(hint).limit(limit).forEach(function (data) {

    var priceDoc;
    var currentDate = new Date("2015-11-26T00:00:00");
    var fromDate;
    
    //Mensajes debug, hay que quitarlos cuando corramos el proceso complet
    //print("==Calculando precio para _id:" + data._id + " activeRevision.@attributes.id:" + data['activeRevision']['@attributes']['id'] + " isbn:" + data['isbn']+"==");



    //Primero revisar el precio que no tiene llave numerica, si coinciden las fechas insertar y regresar
    //print("Revisando precio sin llave numerica ");
    //printjson(data["activeRevision"]["prices"]["price"]["@attributes"]);
    //printjson(data["activeRevision"]["prices"]["price"]["listprice"]);
    //printjson(data["activeRevision"]["prices"]["price"]["sellingprice"]);

    //Maneja el caso que el precio sea el que no tiene llave numerica
    priceDoc = handlePrice(data, data["activeRevision"]["prices"]["price"], currentDate);
    if (priceDoc !== false) {
        db.day_prices.save(priceDoc);
        return;
    }


//Si no coincide entonces comenzamos a checar los precios que tienen llave numerica, uno por uno checamos si la
//llave existe y checamos si al fecha coincide, el primero que ocincide insertamos y regresamos
//Este for acaba si no esta definida alguna llave 0, 1, 2, etc..
    for (var i = 0; typeof data["activeRevision"]["prices"]["price"][i] !== "undefined"; i++) {
        //print("Revisando precio " + i);
        //printjson(data["activeRevision"]["prices"]["price"][i]);

        //Si alguno coindice insertamos y regresamos
        priceDoc = handlePrice(data, data["activeRevision"]["prices"]["price"][i], currentDate);
    if (priceDoc !== false) {
        db.day_prices.save(priceDoc);
        return;
    }


    }



});

var end_time = new Date().getTime();
print('Elapsed: ', (end_time - start_time) / 1000, ' s');
