
/***** CONFIG ****/
var query = {
    "activeRevision.@attributes.id": {
        "$exists": true
    }
};

var hint = {
    "activeRevision.@attributes.id": 1
};

//set to 0 for no limit
var limit = 0;

/********************/

/*Limpiamos la colección*/

db.today_prices.remove({});



// Revisa si el libro es gratuito y lo inserta con 0
// Si cae en este caso regresa true
function handleFree(data) {

    var priceDoc;

    if (data["activeRevision"]["prices"]["price"]["sellingprice"] === "0.00") {
        //print("Libro Gratuito");

        //Si es gratis insertamos en 0
        priceDoc = {
            'active_revision_id': data['activeRevision']['@attributes']['id'],
            'title': data['activeRevision']['title'],
            'language':data['activeRevision']['language'],
            'isbn': data['isbn'],
            'listprice':  parseFloat("0.00"),
            'sellingprice':  parseFloat("0.00")
        };

        return priceDoc;
    }

    return false;
}


// Revisa si el que no tiene llave numerica es el actual
// Si es el actual regresa un objeto
//Si no es regresa false
function handlePrice(data, price, currentDate) {

    //Todos tienen fromDate
    var fromDate = new Date(price["@attributes"]["from"]);
    var toDate;

    //Checamos si tiene toDate
    if (typeof price["@attributes"]["to"] !== "undefined") {

        //Obtenemos el toDate
        toDate = new Date(price["@attributes"]["to"]);

        //comparamos la fecha
        if (currentDate >= fromDate && currentDate <= toDate) {

            //print("Esta en el rango from-to del precio");

            priceDoc = {
                'active_revision_id': data['activeRevision']['@attributes']['id'],
                'title': data['activeRevision']['title'],
                'language':data['activeRevision']['language'],
                'isbn': data['isbn'],
                'listprice':  parseFloat(price["listprice"]["@value"]),
                'sellingprice':  parseFloat(price["sellingprice"]["@value"]),
                'preorder': price["@attributes"]["isPreOrder"]
            };
            return priceDoc;
        } else {
            return false;
        }

    } else { // si no tiene toDate solo checamos el from

        //comparamos la fecha
        if (currentDate >= fromDate) {

            //print("Es mayor al from del precio , insertando...");

            priceDoc = {
                'active_revision_id': data['activeRevision']['@attributes']['id'],
                'title': data['activeRevision']['title'],
                'language':data['activeRevision']['language'],
                'isbn': data['isbn'],
                'listprice':  parseFloat(price["listprice"]["@value"]),
                'sellingprice':  parseFloat(price["sellingprice"]["@value"]),
                'preorder': price["@attributes"]["isPreOrder"]
            };
            return priceDoc;
        } //end if
    } //end else

    return false;


}//end handlePrice

//Start time for profiling
var start_time = new Date().getTime();

//Main update function
db.books.find(query).hint(hint).limit(limit).forEach(function (data) {

    var priceDoc;
    var currentDate = new Date();
    var fromDate;
    var toDate;

    //Mensajes debug, hay que quitarlos cuando corramos el proceso complet
    //print("==Calculando precio para _id:" + data._id + " activeRevision.@attributes.id:" + data['activeRevision']['@attributes']['id'] + " isbn:" + data['isbn']+"==");


    //Maneja el caso gratuito.
    priceDoc = handleFree(data);
    if (priceDoc !== false) {
        db.today_prices.save(priceDoc);
        return;
    }

    //Primero revisar el precio que no tiene llave numerica, si coinciden las fechas insertar y regresar
    //print("Revisando precio sin llave numerica ");
    //printjson(data["activeRevision"]["prices"]["price"]["@attributes"]);
    //printjson(data["activeRevision"]["prices"]["price"]["listprice"]);
    //printjson(data["activeRevision"]["prices"]["price"]["sellingprice"]);

    //Maneja el caso que el precio sea el que no tiene llave numerica
    priceDoc = handlePrice(data, data["activeRevision"]["prices"]["price"], currentDate);
    if (priceDoc !== false) {

        //insertar priceDoc
        db.today_prices.save(priceDoc);
        return;
    }

//Si no coincide entonces comenzamos a checar los precios que tienen llave numerica, uno por uno checamos si la
//llave existe y checamos si al fecha coincide, el primero que ocincide insertamos y regresamos
//Este for acaba si no esta definida alguna llave 0, 1, 2, etc..
    for (var i = 0; typeof data["activeRevision"]["prices"]["price"][i] !== "undefined"; i++) {
        //print("Revisando precio " + i);
        //printjson(data["activeRevision"]["prices"]["price"][i]);

        //Si alguno coindice insertamos y regresamos
        priceDoc = handlePrice(data, data["activeRevision"]["prices"]["price"][i], currentDate);
        if (priceDoc !== false) {

            //insertar priceDoc
            db.today_prices.save(priceDoc);
            return;
        }

    }


//Si ninguno coincide, entonces insertamos -1 en el precio, esto quier decir que el libro deberia estar desactivado,
//ya que no pudimos determinar el precio para el dia de hoy.
    //print("No se pudo determinar el precio");
    priceDoc = {
        'active_revision_id': data['activeRevision']['@attributes']['id'],
        'title': data['activeRevision']['title'],
        'language':data['activeRevision']['language'],
        'isbn': data['isbn'],
        'listprice': -1,
        'sellingprice': -1,
        'preorder': -1
    };

    //Aqui insertamos a la otra coleccion
    db.today_prices.save(priceDoc);
});

var end_time = new Date().getTime();
print('Elapsed: ', (end_time - start_time) / 1000, ' s');

/********************/

/*Limpiamos la colección*/

db.future_prices.remove({});


function handlePrice(data, price, futureDate) {

    //Todos tienen fromDate
    var fromDate = new Date(price["@attributes"]["from"]);


    //comparamos la fecha
    if (futureDate <= fromDate) {

        //print("Es mayor al from del precio , insertando...");

        priceDoc = {
            'active_revision_id': data['activeRevision']['@attributes']['id'],
            'title': data['activeRevision']['title'],
            'language':data['activeRevision']['language'],
            'isbn': data['isbn'],
            'listprice': parseFloat(price["listprice"]["@value"]),
            'sellingprice': parseFloat(price["sellingprice"]["@value"]),
            'fromdate': price["@attributes"]["from"],
            'preorder': price["@attributes"]["isPreOrder"]
        };
        return priceDoc;
    } //end if


    return false;


}//end handlePrice

//Start time for profiling
var start_time = new Date().getTime();

//Main update function
db.books.find(query).hint(hint).limit(limit).forEach(function (data) {

    var priceDoc;
    var futureDate = new Date();
    futureDate.setDate(futureDate.getDate() + 7);




    //Maneja el caso que el precio sea el que no tiene llave numerica
    priceDoc = handlePrice(data, data["activeRevision"]["prices"]["price"], futureDate);
    if (priceDoc !== false) {

        //insertar priceDoc
        db.future_prices.save(priceDoc);
        return;
    }

//Si no coincide entonces comenzamos a checar los precios que tienen llave numerica, uno por uno checamos si la
//llave existe y checamos si al fecha coincide, el primero que ocincide insertamos y regresamos
//Este for acaba si no esta definida alguna llave 0, 1, 2, etc..
    for (var i = 0; typeof data["activeRevision"]["prices"]["price"][i] !== "undefined"; i++) {


        //Si alguno coindice insertamos y regresamos
        priceDoc = handlePrice(data, data["activeRevision"]["prices"]["price"][i], futureDate);
        if (priceDoc !== false) {

            //insertar priceDoc
            db.future_prices.save(priceDoc);
            return;
        }

    }
});

var end_time = new Date().getTime();
print('Elapsed: ', (end_time - start_time) / 1000, ' s');

/***Limpiamos la colección***/

var start_time = new Date().getTime();

db.future_prices_compare.remove({});

db.future_prices.find().limit(limit).forEach(
    function(doc) {
        data = db.today_prices.find({'active_revision_id': doc['active_revision_id']}).toArray();
        priceDoc = {
            'active_revision_id': doc['active_revision_id'],
            'title': doc['title'],
            'isbn': doc['isbn'],
            'future_list_price':  parseFloat(doc['listprice']),
            'future_selling_price':  parseFloat(doc['sellingprice']),
            'future_price_from_date': doc['fromdate'],
            'future_ispreorder': doc['preorder'],
            'today_list_price':  parseFloat(data[0]['listprice']),
            'today_selling_price':  parseFloat(data[0]['sellingprice'])
        };
        db.future_prices_compare.save(priceDoc);
    }
);

var end_time = new Date().getTime();
print('Elapsed: ', (end_time - start_time) / 1000, ' s');