/***** CONFIG ****/

//set to 0 for no limit
var limit = 0;

/********************/

/*Limpiamos la colección*/

db.sales_id.remove({});

db.sales.find().limit(limit).forEach(
    function(doc) {
        data = db.books.find({'activeRevision.@attributes.id': doc['id']}).toArray();
        db.sales_id.save(data);
    }
);

db.kids.remove({});

firstsearch= db.sales_id.find({'activeRevision.isKids': 'true'}).toArray();
db.kids.save(firstsearch);

db.top_kids_sales.remove({});

db.kids.find().limit(limit).forEach(
    function(doc) {
        data = db.sales.find({'id': doc['activeRevision']['@attributes']['id']}).toArray();
        db.top_kids_sales.insert(data);
    }
);