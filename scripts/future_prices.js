/********************

 Script para calcular los precios a futuro de la coleccion "books"

 Para probar:
 1. Conectate al servider de orbiletest con putty
 2. Actualiza el contenido de /home/ubuntu/price_update.js
 3. Conectarse a mongo mongo ds053582-a0.mongolab.com:53582/orbile_production -u orbile_production -p
 4. En la consola de mongo escribir load("future_prices.js")

 Antes de correr el proceso sin el limit(5) asegurarse de comentar todos los mensajes debug: //print y //printjson
 *******************/
/***** CONFIG ****/
var query = {
    "activeRevision.@attributes.id": {
        "$exists": true
    }
};

var hint = {
    "activeRevision.@attributes.id": 1
};

//set to 0 for no limit
var limit = 0;

/********************/

/*Limpiamos la colección*/

db.future_prices.remove({});


function handlePrice(data, price, futureDate) {

    //Todos tienen fromDate
    var fromDate = new Date(price["@attributes"]["from"]);


        //comparamos la fecha
        if (futureDate <= fromDate) {

            //print("Es mayor al from del precio , insertando...");

            priceDoc = {
                'active_revision_id': data['activeRevision']['@attributes']['id'],
                'title': data['activeRevision']['title'],
                'language':data['activeRevision']['language'],
                'isbn': data['isbn'],
                'listprice': parseFloat(price["listprice"]["@value"]),
                'sellingprice': parseFloat(price["sellingprice"]["@value"]),
                'fromdate': price["@attributes"]["from"],
                'preorder': price["@attributes"]["isPreOrder"]
            };
            return priceDoc;
        } //end if


    return false;


}//end handlePrice

//Start time for profiling
var start_time = new Date().getTime();

//Main update function
db.books.find(query).hint(hint).limit(limit).forEach(function (data) {

    var priceDoc;
    var futureDate = new Date();
    futureDate.setDate(futureDate.getDate() + 7);




    //Maneja el caso que el precio sea el que no tiene llave numerica
    priceDoc = handlePrice(data, data["activeRevision"]["prices"]["price"], futureDate);
    if (priceDoc !== false) {

        //insertar priceDoc
        db.future_prices.save(priceDoc);
        return;
    }

//Si no coincide entonces comenzamos a checar los precios que tienen llave numerica, uno por uno checamos si la
//llave existe y checamos si al fecha coincide, el primero que ocincide insertamos y regresamos
//Este for acaba si no esta definida alguna llave 0, 1, 2, etc..
    for (var i = 0; typeof data["activeRevision"]["prices"]["price"][i] !== "undefined"; i++) {


        //Si alguno coindice insertamos y regresamos
        priceDoc = handlePrice(data, data["activeRevision"]["prices"]["price"][i], futureDate);
        if (priceDoc !== false) {

            //insertar priceDoc
            db.future_prices.save(priceDoc);
            return;
        }

    }
});

var end_time = new Date().getTime();
print('Elapsed: ', (end_time - start_time) / 1000, ' s');


