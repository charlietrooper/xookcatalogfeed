<?php

/**
 * Created by PhpStorm.
 * User: CarlosMiranda
 * Date: 19/12/2016
 * Time: 9:16
 */
class Elasticsearch
{

    public $endpoint;

    function __construct($endpoint)
    {
        $this->endpoint = $endpoint;
        $this->client = curl_init($this->endpoint);
        curl_setopt($this->client, CURLOPT_CUSTOMREQUEST, "POST");

    }

    function QueryBuilder($search,$type,$text,$category, $initialrange, $finalrange, $language, $page, $itemsperpage)
    {
        $size = $page * $itemsperpage;
        if ($initialrange=="ALL" & $finalrange=="ALL")
        {
            if($category=="ALL")
            {
                if($language=="ALL") {
                    $queryData = array("from" => $size, "size" => $itemsperpage, "query" => array($search => array($type => $text)));
                }
                else{
                    $queryData = array("from" => $size, "size" => $itemsperpage, "query" => array("bool"=>array("must"=>[array($search => array($type => $text)),array("match" => array("language" => $language))])));
                }
            }
            else
            {
                if($language=="ALL")
                {
                    $queryData = array("from" => $size, "size" => $itemsperpage, "query" => array("bool"=>array("must"=>[array($search => array($type => $text)),array("match" => array("category" => $category))])));

                }
                else{
                    $queryData = array("from" => $size, "size" => $itemsperpage, "query" => array("bool"=>array("must"=>[array($search => array($type => $text)),array("match" => array("category" => $category)),array("match" => array("language" => $language))])));
                }
            }
        }
        else
        {
            if($category=="ALL") {
                if ($language == "ALL") {
                    $queryData = array("from" => $size, "size" => $itemsperpage, "query" => array("bool" => array("must" => [array($search => array($type => $text)), array("range" => array("sellingprice" => array("gte" => $initialrange, "lte" => $finalrange)))])));

                }
                else{
                    $queryData = array("from" => $size, "size" => $itemsperpage, "query" => array("bool" => array("must" => [array($search => array($type => $text)),array("match" => array("language" => $language)), array("range" => array("sellingprice" => array("gte" => $initialrange, "lte" => $finalrange)))])));
                }
            }
            else
            {
                if($language=="ALL") {
                    $queryData = array("from" => $size, "size" => $itemsperpage, "query" => array("bool" => array("must" => [array($search => array($type => $text)), array("match" => array("category" => $category)), array("range" => array("sellingprice" => array("gte" => $initialrange, "lte" => $finalrange)))])));
                }
                else{
                    $queryData = array("from" => $size, "size" => $itemsperpage, "query" => array("bool" => array("must" => [array($search => array($type => $text)), array("match" => array("category" => $category)),array("match" => array("language" => $language)), array("range" => array("sellingprice" => array("gte" => $initialrange, "lte" => $finalrange)))])));
                }
            }
        }
        return $queryData;
    }


    function titleTermSearch($title,$category, $initialrange, $finalrange, $language, $page, $itemsperpage)
    {
        $queryData = self::QueryBuilder("term","title",$title,$category, $initialrange, $finalrange, $language, $page, $itemsperpage);
        $data_string = json_encode($queryData);
        curl_setopt($this->client, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($this->client, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->client, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        $resultado = curl_exec($this->client);
        return $resultado;

    }

    //
    function authorTermSearch($author,$category, $initialrange, $finalrange, $language, $page, $itemsperpage)
    {
        $queryData = self::QueryBuilder("term","author",$author,$category, $initialrange, $finalrange, $language, $page, $itemsperpage);
        $data_string = json_encode($queryData);
        curl_setopt($this->client, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($this->client, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->client, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        $resultado = curl_exec($this->client);
        return $resultado;
    }


    function titleMatchSearch($title,$category, $initialrange, $finalrange, $language, $page, $itemsperpage)
    {
        $queryData = self::QueryBuilder("match","title",$title,$category, $initialrange, $finalrange, $language, $page, $itemsperpage);
        $data_string = json_encode($queryData);
        curl_setopt($this->client, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($this->client, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->client, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        $resultado = curl_exec($this->client);
        return $resultado;
    }

    function authorMatchSearch($author,$category, $initialrange, $finalrange, $language, $page, $itemsperpage)
    {
        $queryData = self::QueryBuilder("match","author",$author,$category, $initialrange, $finalrange, $language, $page, $itemsperpage);
        $data_string = json_encode($queryData);
        curl_setopt($this->client, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($this->client, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->client, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        $resultado = curl_exec($this->client);
        return $resultado;
    }
}